#    Copyright (C) 2004 Laurence Reading

#    This file is part of ProjectAwesomeModBot.
#
#    ProjectAwesomeModBot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ProjectAwesomeModBot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ProjectAwesomeModBot.  If not, see <http://www.gnu.org/licenses/>.

import ConfigParser
import datetime
import logging
import xml.etree.ElementTree as ET
import sys
import RedditAPIFacade

__author__ = 'lreading'


def dayToNum(dayNum):
    return {
        'Monday': 1,
        'Tuesday': 2,
        'Wednesday': 3,
        'Thursday': 4,
        'Friday': 5,
        'Saturday': 6,
        'Sunday': 7,
    }[dayNum]


def findNextDay(day):
    today = datetime.datetime.utcnow().isoweekday()
    diff = day - today
    if (diff < 0):
        diff += 7
    delta = datetime.timedelta(days=diff)
    return datetime.datetime.utcnow().date() + delta


def extractEventDetails(eventXML, calDetailsTag, calendarDetailsFormat):
    eventDates = []
    calDetails = ""
    game = eventXML.find('Game').text
    for eventSchedule in eventXML.findall('EventSchedule'):
        day = eventSchedule.find('day').text

        #determine next event date
        eventDate = findNextDay(dayToNum(day))
        eventTime = datetime.datetime.strptime(eventSchedule.find('time').text, '%H:%M').time()
        eventDateTime = datetime.datetime.combine(eventDate, eventTime)
        calDetails += calDetailsTag + '|' + eventDateTime.strftime(
            calendarDetailsFormat) + '|' + game + '|' + eventXML.find('Title').text + '|  \n'
        eventDateDetails = {'date': eventDate, 'day': day, 'time': eventSchedule.find('time').text}
        eventDates.append(eventDateDetails)
    #make post about event
    text = eventXML.find('SelfText').text + '\n\n' + calDetails

    title = eventXML.find('Title').text + ' - ' + eventDates[0]['day']
    time = eventDates[0]['time']
    oneTime = True
    if len(eventDates) > 1:
        i = 1
        while i < (len(eventDates) - 1):
            if eventDates[i]['time'] != time:
                oneTime = False
            title += ', ' + eventDates[i]['day']
            i += 1
        if eventDates[i]['time'] != time:
            oneTime = False
        title += ' and ' + eventDates[i]['day']
    if oneTime:
        title += ' ' + time + ' UTC'
    return {'text': text, 'title': title, 'dates': eventDates}


def createEvent(eventDetails, auth, subreddit):
    response = RedditAPIFacade.makeSelfPost(eventDetails['title'], eventDetails['text'], auth, subreddit)
    #flair post
    name = response.json()['json']['data']['name']
    RedditAPIFacade.flairLink(name, auth, subreddit, css='EVENT', text='EVENT')


logger = logging.getLogger("redditbot")
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler('redditbot.log')
handler.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
ch.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(handler)
logger.info("Running Auto Event Manager")
try:
    #load settings
    managerConfig = ConfigParser.RawConfigParser()
    managerConfig.read('EventManagerSettings.cfg')
    subreddit = managerConfig.get('Config', 'subreddit')
    username = managerConfig.get('Auth', 'username')
    password = managerConfig.get('Auth', 'password')
    eventConfig = ConfigParser.RawConfigParser()
    eventConfig.read('settings.cfg')
    calendarDetailsTag = eventConfig.get('Config', 'caldetails')
    calendarDetailsFormat = eventConfig.get('Config', 'inputdateformat')
    events = {}

    #load event file
    tree = ET.parse('events.xml')
    root = tree.getroot()
    if (root.tag != 'events'):
        pass  #throw
    for child in root:
        today = datetime.datetime.utcnow().isoweekday()
        todaysDate = datetime.datetime.utcnow().strftime("%Y-%m-%d")
        logger.debug("Found event to be posted on " + child.find('PostSchedule').text)
        postDay = dayToNum(child.find('PostSchedule').text)
        if today == postDay and child.find('LastPost').text != todaysDate:
            logger.info("Creating event: " + child.find('Title').text)
            #create events
            details = extractEventDetails(child, calendarDetailsTag, calendarDetailsFormat)
            createEvent(details, {'user': username, 'pass': password}, subreddit)
            #update xml
            child.find('LastPost').text = todaysDate
    tree.write('events.xml')
except:
    formatter = logging.Formatter
    logger.critical(datetime.datetime.utcnow())
    logger.critical(formatter.formatException(formatter, ei=sys.exc_info()))


