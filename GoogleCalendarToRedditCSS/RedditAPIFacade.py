#    Copyright (C) 2004 Laurence Reading

#    This file is part of ProjectAwesomeModBot.
#
#    ProjectAwesomeModBot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ProjectAwesomeModBot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ProjectAwesomeModBot.  If not, see <http://www.gnu.org/licenses/>.

import time

__author__ = 'lreading'

import requests
import HTMLParser


def getStylesheet(subreddit):
    headers = {'User-agent': 'Project Awesome Mod Bot by /u/imreading'}
    style = requests.get(r'https://www.reddit.com/r/' + subreddit + '/about/stylesheet/.json', headers=headers)
    return HTMLParser.HTMLParser().unescape(style.json()['data']['stylesheet'])


def updateStylesheet(subreddit, style, auth):
    data = {'api_type': 'json', 'passwd': auth['pass'], 'rem': 'false', 'user': auth['user']}
    p = requests.post(r'https://www.reddit.com/api/login', data=data)
    r = requests.get(r'https://www.reddit.com/api/me.json', cookies=p.cookies)
    data = {'api_type': 'json', 'op': 'save', 'stylesheet_contents': style}
    headers = {'X-Modhash': r.json()['data']['modhash'], 'User-agent': 'Project Awesome Mod Bot by /u/imreading'}
    p = requests.post(r'https://www.reddit.com/r/' + subreddit + '/api/subreddit_stylesheet', data=data, headers=headers,
                      cookies=p.cookies)


def getSidebar(subreddit, auth):
    data = {'api_type': 'json', 'passwd': auth['pass'], 'rem': 'false', 'user': auth['user']}
    p = requests.post(r'https://www.reddit.com/api/login', data=data)
    r = requests.get(r'https://www.reddit.com/api/me.json', cookies=p.cookies)
    headers = {'X-Modhash': r.json()['data']['modhash'], 'User-agent': 'Project Awesome Mod Bot by /u/imreading'}
    response = requests.get(r'https://www.reddit.com/r/' + subreddit + r'/about/edit.json', headers=headers,
                            cookies=p.cookies)
    return response.json()['data']['description']


def getSession(auth, retries=0):
    if (retries == 4):
        raise Exception("Failed to Authenticate")
    headers = {'Connection': 'close', 'User-agent': 'Project Awesome Mod Bot by /u/imreading'}
    data = {'api_type': 'json', 'passwd': auth['pass'], 'rem': 'false', 'user': auth['user']}
    try:
        session = requests.session()
        session.headers=headers
        p = session.post(r'https://ssl.reddit.com/api/login', data=data)
        modhash = p.json()['json']['data']['modhash']
    except:
        time.sleep(2)
        return getSession(auth, retries + 1)
    session.headers.update({'X-Modhash': modhash})
    return session


def getSidebar(subreddit, auth, retries=0):
    if (retries == 4):
        raise Exception("Failed to read current sidebar")
    session = getSession(auth)
    try:
        description = \
            session.get(r'https://www.reddit.com/r/' + subreddit + r'/about/edit.json').json()['data']['description']
    except:
        time.sleep(2)
        return getSidebar(subreddit, auth, retries + 1)
    return HTMLParser.HTMLParser().unescape(description)


def setSidebar(subreddit, sidebar, auth, retries=0):
    if (retries == 4):
        raise Exception("Failed to read current subreddit settings, check permissions?")
    session = getSession(auth)
#    try:
    current = session.get(r'https://www.reddit.com/r/' + subreddit + r'/about/edit.json')
    currentJson = current.json()
    data = setAdminData(currentJson)
#    except:
#        time.sleep(2)
#        return setSidebar(subreddit, sidebar, auth, retries + 1)
    data['description'] = sidebar
    p = session.post(r'https://www.reddit.com/api/site_admin', data=data)


def getPost(postID, retries=0):
    if (retries == 4):
        raise Exception("Failed to read post contents")
    try:
        headers = {'User-agent': 'Project Awesome Mod Bot by /u/imreading'}
        contents = \
            requests.get(r'https://www.reddit.com/' + postID + '/.json', headers=headers).json()[0]['data']['children'][
                0][
                'data']['selftext']
        contents  = HTMLParser.HTMLParser().unescape(contents)
    except:
        time.sleep(2)
        return getPost(postID, retries + 1)
    return contents


def setAdminData(editjson):
    original = editjson['data']
    data = {'api_type': 'json', 'comment_score_hide_mins': original['comment_score_hide_mins'],
            'description': original['description'],
            'exclude_banned_modqueue': original['exclude_banned_modqueue'], 'lang': original['language'],
            'link_type': original['content_options'], 'over_18': original['over_18'],
            'public_description': original['public_description'],
            'public_traffic': original['public_traffic'], 'show_media': original['show_media'],
            'spam_comments': original['spam_comments'],
            'spam_links': original['spam_links'], 'spam_selfposts': original['spam_selfposts'],
            'sr': original['subreddit_id'],
            'submit_link_label': original['submit_link_label'], 'submit_text': original['submit_text'],
            'submit_text_label': original['submit_text_label'], 'title': original['title'],
            'type': original['subreddit_type'],
            'wiki_edit_age': original['wiki_edit_age'], 'wiki_edit_karma': original['wiki_edit_karma'],
            'wikimode': original['wikimode'],
            'css_on_cname': 'true', 'allow_top': 'true', 'header-title': 'a string no longer than 500 characters',
            'show_cname_sidebar': 'false', 'show_media_preview': original['show_media_preview'], 'allow_discovery': original['allow_discovery'], 'submit_text_label': original['submit_text_label'],
            'submit_link_label': original['submit_link_label'],
            'spoilers_enabled': original['spoilers_enabled'], 'free_form_reports': original['free_form_reports'], 'allow_images': original['allow_images'],}
    return data


def comment(comment, postId, auth, distinguish=False):
    session = getSession(auth)
    data = {'text': comment, 'parent': postId}
    p = session.post(r'https://www.reddit.com/api/comment', data=data)
    if (distinguish):
        start = p.text.find('"id": "') + '"id": "'.__len__()
        end = p.text[start:].find('"')
        id = p.text[start:end + start]
        data = {'api_type': 'json', 'how': 'yes', 'id': id}
        p = session.post(r'https://www.reddit.com/api/distinguish', data=data)


def makeSelfPost(title, selfText, auth, subreddit):
    session = getSession(auth)
    data = {'api_type': 'json', 'text': comment, 'kind': 'self', 'sr': subreddit, 'text': selfText, 'title': title}
    p = session.post(r'https://www.reddit.com/api/submit', data=data)
    return p


def flairLink( fullName, auth,subreddit,css=None,text=None):
    session = getSession(auth)
    data = {'api_type': 'json', 'link': fullName}
    if (css):
        data.update({'css_class':css})
    if (text):
        data.update({'text':text})
    p = session.post(r'https://www.reddit.com/r/'+subreddit+'/api/flair', data=data)
    return p

def updatePost(comment, postId, auth):
    session = getSession(auth)
    data = {'text': comment, 'thing_id': postId}
    session.post(r'https://www.reddit.com/api/editusertext', data=data)


def getComments(username):
    headers = {'User-agent': 'Project Awesome Mod Bot by /u/imreading'}
    comments = requests.get(r'https://www.reddit.com/user/' + username + r'/comments/.json', headers=headers)
    return comments
