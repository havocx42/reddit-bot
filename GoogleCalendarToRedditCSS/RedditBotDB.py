__author__ = 'Laurence'
import peewee
import datetime
from peewee import *

db = MySQLDatabase('modbot', user='modbot', passwd='modbotsucks')


class Event(peewee.Model):
    name = peewee.CharField()
    game = peewee.CharField()
    subreddit = peewee.CharField()
    date = peewee.DateTimeField()
    postID = peewee.CharField()
    creationTweeted = peewee.BooleanField()
    startingTweeted = peewee.BooleanField()

    class Meta:
        database = db
        indexes = (
        (('date', 'name'), True),
        )


db.create_tables([Event], safe=True)


class RedditBotDB:
    def __init__(self, logger):
        self.logger = logger

    def saveEvents(self, efp, sub):
        events = list(efp)
        currentEvents = Event.filter(Event.date > datetime.datetime.utcnow().replace(microsecond=0) and Event.subreddit == sub.upper())
        unmatchedEvents = list(currentEvents)
        for event in events:
            match = False
            for e in currentEvents:
                if e.name == event['title'] and e.date == event['date']:
                    match = True
                    self.logger.debug("Found matching event for " + event['title'] + " in post: " + e.postID)
                    unmatchedEvents.remove(e)
            if (not match):
                newEvent = Event(name=event['title'], game=event['game'], date=event['date'], postID=event['postID'],
                                 creationTweeted=False, startingTweeted=False, subreddit=sub.upper())
                try:
                    newEvent.save()
                except IntegrityError:
                    pass
        for uEvent in unmatchedEvents:
            self.logger.debug("Deleting event in postID:" + uEvent.postID + " Event Name:" + uEvent.name)
            uEvent.delete_instance()
