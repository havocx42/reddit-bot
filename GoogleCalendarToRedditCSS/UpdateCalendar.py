# Copyright (C) 2004 Laurence Reading

# This file is part of ProjectAwesomeModBot.
#
#    ProjectAwesomeModBot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ProjectAwesomeModBot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ProjectAwesomeModBot.  If not, see <http://www.gnu.org/licenses/>.

import sys
import RedditAPIFacade
import datetime
import logging
import ConfigParser
import requests
import RedditBotUtils
import RedditBotDB
import GoogleCalendar
from raven import Client

__author__ = 'havocx42'

#logging.basicConfig(filename='redditbot.log',level=logging.DEBUG)
logger = logging.getLogger("redditbot")
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler('redditbot.log')
handler.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
ch.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(handler)
db = RedditBotDB.RedditBotDB(logger)
client = Client('https://00d4ae4d65b14528a3c024b77a59bc9b:e74ce118360647cc9e3d91d82fa801e7@app.getsentry.com/35607')
try:
    config = ConfigParser.RawConfigParser()
    config.read('settings.cfg')
    subreddits = config.get('Config', 'subreddits').splitlines()
    enable_sentry_logging = config.getboolean('Config', 'sentry')
    starttag = config.get('Config', 'starttag')
    endtag = config.get('Config', 'endtag')
    calendar_ID = config.get('Config', 'calendar')
    username = config.get('Auth', 'username')
    password = config.get('Auth', 'password')
    calendarDetailsTag = config.get('Config', 'caldetails')
    inputDateFormat = config.get('Config', 'inputdateformat')
    calendarFullDateFormat = config.get('Config', 'calendarFullDateFormat')
    calendarDateOnlyFormat = config.get('Config', 'calendarDateOnlyFormat')
    calendarTimeFormat = config.get('Config', 'calendarTimeFormat')
    incorrectCalendarComment = "Please provide calendar details in event posts.  \nIt should look like this:  \n" + calendarDetailsTag + "|2014-12-25 20:00|Game(e.g. ARMA 3)| event name|"
    enableComments = config.getboolean('Config', 'enableComments')
    updateEventPost = config.getboolean('Config', 'updateEventPost')

    updatedPrefix = config.get('Config', 'updatedPrefix')
    ignoredPosts = config.get('Ignored', 'ignoredEvents').split(',')
    today = datetime.datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0)
    for subreddit_and_colour in subreddits:
        separated = subreddit_and_colour.split('/')
        subreddit=separated[0]
        logger.info("Updating Calendar for subreddit: "+subreddit)
        cal_colour=separated[1]
        sticky=None
        if(len(separated)>2):
            sticky = separated[2]
        #Find the event flaired items on the front page
        headers = {'User-agent': 'Project Awesome Mod Bot by /u/imreading'}
        data = requests.get(
            r'http://www.reddit.com/r/' + subreddit + '/search.json?q=flair%3AEVENT&sort=new&restrict_sr=on',
            headers=headers)
        results = RedditBotUtils.findEventDetails(data, calendarDetailsTag, inputDateFormat)
        events = results['events']
        errors = results['errors']
        #If there is an error search for a comment by this username
        if enableComments:
            for postId in errors:
                if not postId in ignoredPosts:
                    if not RedditBotUtils.isPostCommentedOn(username, postId):
                        #If none found create comment asking them to add calendar details to their selfpost
                        logger.info(
                            "Commenting on post: " + postId + " because it's an event post but failed to read the calendar details")
                        RedditAPIFacade.comment(incorrectCalendarComment, postId, {'user': username, 'pass': password},
                                                True)
                    else:
                        logger.debug("Already commented on post: " + postId)
                else:
                    logger.debug("Ignoring post: " + postId)

        #Sort and filter resulting events
        sortedEvents = sorted(events, key=lambda event: event['date'])
        filteredEvents = []
        for event in sortedEvents:
            if event['date'] > today and not 't3_' + event['postID'] in ignoredPosts:
                filteredEvents.append(event)
        #create text of sidebar calendar
        table = ""
        try:
            db.saveEvents(filteredEvents,subreddit)
        except:
            if enable_sentry_logging:
                client.captureException()
            logger.exception("Failed to save events to DB")
        try:
            GoogleCalendar.updateGoogleCalendar(calendar_ID, filteredEvents,
                                               subreddit, cal_colour)
        except:
            if enable_sentry_logging:
                client.captureException()
            logger.exception("Failed to store events to google calendar")
        for event in filteredEvents:
            style = ""
            if event['date'].date() == today.date():
                style = "**"
            date = event['date']
            game = event['game'].strip()
            minutes = (date.hour - 12) * 60 + date.minute
            timelink = "http://emoji-emoticons.com/wp-content/uploads/2015/07/ShrugEmoticon-.jpg"
            if 'calLink' in event.keys():
                timelink = event['calLink']
            else:
                logger.error("unable to find Calendar link")
            table += '|[' + style + event['date'].strftime(
                calendarDateOnlyFormat) + style + '](' + timelink + ')|[' + style + event[
                         'date'].strftime(
                calendarTimeFormat) + ' UTC' + style + '](' + timelink + ')|' + style + '[' + game + ']' + style + '|[' + style + \
                     event['title'].strip() + style + '](' + \
                     event[
                         'url'] + ')\n'
        #Add to sidebar
        desc = RedditAPIFacade.getSidebar(subreddit, {'user': username, 'pass': password})
        header = "\n\n>DATE|TIME|GAME|EVENT\n>:--|--|--|--:\n"
        footerEnd = " UTC*\n\n"
        footer = "*"+updatedPrefix + " " + datetime.datetime.utcnow().replace(microsecond=0).strftime(
            calendarFullDateFormat) + footerEnd

        if updateEventPost and sticky:
            currentContents = RedditAPIFacade.getPost(sticky)
            start = currentContents.find(header)
            end = currentContents.find(updatedPrefix)
            end = end + currentContents[end:].find(footerEnd) + len(footerEnd)
            if -1 != start and -1 != end:
                newContents = currentContents[:start] + header + table + footer + currentContents[end:]
                RedditAPIFacade.updatePost(newContents, 't3_' + sticky, {'user': username, 'pass': password})
            else:
                pass
        try:
            calNeedsUpdate = not RedditBotUtils.compareBetween(desc, starttag, updatedPrefix, header + table)
        except:
            logger.exception("Error comparing old calendar")
            calNeedsUpdate = True

        if calNeedsUpdate:
            newdesc = RedditBotUtils.replaceBetween(desc, starttag, endtag, header + table + footer)
            RedditAPIFacade.setSidebar(subreddit, newdesc, {'user': username, 'pass': password})
            logger.info("Successfully updated calendar!")
        else:
            logger.info("Calendar already up to date!")
except:
    formatter = logging.Formatter
    if enable_sentry_logging:
        client.captureException()
    print('An error occurred, Logging')
    logger.exception(datetime.datetime.utcnow())


