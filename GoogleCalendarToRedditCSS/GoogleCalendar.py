import re
import sys
import datetime
import arrow
import pytz
import logging
from dateutil import tz
from oauth2client import client
from googleapiclient import sample_tools


def main(argv):
    # Authenticate and construct service.
    calendarID = 'rhcncdjr1nj3hflce8ilqchf10@group.calendar.google.com'
    service = getService()

    try:
        new_event = {
            'summary': "an Event",
            'description': "ProjectMilSim - http://reddit.com/2vvzei",
            'colorId': "4",
            'start': {
                'dateTime': "2015-06-03T13:00:00Z"
            },
            'end': {
                'dateTime': "2015-06-03T13:00:00Z"
            },
        }
        # createEvent(service, calendarID, new_event)

        eid = searchEvents(service, datetime.datetime.utcnow(), calendarID)[0]['id']
        print(eid)
        updateEvent(service, calendarID, new_event, eid)

    except client.AccessTokenRefreshError:
        print('The credentials have been revoked or expired, please re-run'
              'the application to re-authorize.')


def getService():
    # Authenticate and construct service.
    service, flags = sample_tools.init(["", "--noauth_local_webserver"], 'calendar', 'v3', __doc__, __file__,
                                       scope='https://www.googleapis.com/auth/calendar')
    return service


def searchEvents(service, startDate, calendar):
    eventList = service.events().list(calendarId=calendar, timeMin=startDate.isoformat()).execute()
    return eventList['items']


def eventIsForSub(googleEvent, subreddit):
    pattern = "(.*) - http://reddit.com/(\w*)"
    result = re.search(pattern, googleEvent['description'])
    if result:
        sub = result.group(1).lower()
        if sub == subreddit.lower():
            return True
    return False


def updateGoogleCalendar(calendar, redditEvents, subreddit, colourID):
    logger = logging.getLogger("redditbot")
    logger.debug("Updating Google calendar for "+subreddit)
    service = getService()
    today = datetime.datetime.utcnow()
    start = datetime.datetime(today.year, today.month, today.day, tzinfo=tz.tzutc())
    all_google_events = searchEvents(service, start, calendar)
    googleEvents = [x for x in all_google_events if eventIsForSub(x,subreddit)]

    matchedGoogleEvents = []
    matchedRedditEvents = []
    for googleEvent in googleEvents:
        for redditEvent in redditEvents:
            if eventsMatch(googleEvent, redditEvent) and redditEvent not in matchedRedditEvents:
                syncEvents(service, calendar, googleEvent, redditEvent, colourID)
                redditEvent['calLink'] = googleEvent['htmlLink']
                matchedGoogleEvents.append(googleEvent)
                matchedRedditEvents.append(redditEvent)
                break
    redditEvents = [x for x in redditEvents if x not in matchedRedditEvents]
    googleEvents = [x for x in googleEvents if x not in matchedGoogleEvents]
    for redditEvent in redditEvents:
        logger.info("Creating Google event:" +redditEvent['title'])
        googleEvent = createEvent(service, calendar, createEventBody(redditEvent, colourID))
        redditEvent['calLink'] = googleEvent['htmlLink']
    for googleEvent in googleEvents:
        logger.info("Deleting Google event:" +googleEvent['summary'])
        deleteEvent(service, calendar, googleEvent)


def eventsMatch(googleEvent, redditEvent):
    pattern = "http://reddit.com/(\w*)"
    result = re.search(pattern, googleEvent['description'])
    if result:
        id = result.group(1).lower()
        if id == redditEvent['postID'].lower() and googleEvent['summary'] == ("["+redditEvent['game']+"] "+redditEvent['title']):
            return True
    return False


def deleteEvent(service, calendar, event):
    deletion = service.events().delete(calendarId=calendar, eventId=event['id']).execute()
    print(deletion)


def syncEvents(service, calendar, googleEvent, redditEvent, colourId):
    googleDate = arrow.get(googleEvent['start']['dateTime']).datetime
    redditDate = redditEvent['date']
    redditDate = pytz.utc.localize(redditDate)
    matches = googleEvent['summary'] == ("["+redditEvent['game']+"] "+redditEvent['title']) and googleDate == redditDate
    if not matches:
        updateEvent(service, calendar, createEventBody(redditEvent, colourId), googleEvent['id'])


def createEventBody(event, colourID):
    description = event['subreddit'] + " - " + "http://reddit.com/" + event['postID']
    name = "["+event['game']+"] "+event['title']
    start = event['date']
    end = start
    new_event = {
        'summary': name,
        'description': description,
        'colorId': colourID,
        'start': {
            'dateTime': start.isoformat() + "Z"
        },
        'end': {
            'dateTime': end.isoformat() + "Z"
        },
    }
    return new_event


def updateEvent(service, calendar, newEventBody, id):
    update = service.events().update(calendarId=calendar, eventId=id, body=newEventBody).execute()


def createEvent(service, calendar, eventBody):
    creation = service.events().insert(calendarId=calendar, body=eventBody).execute()
    return creation


if __name__ == '__main__':
    main(sys.argv)
