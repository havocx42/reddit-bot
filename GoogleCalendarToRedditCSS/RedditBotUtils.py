# Copyright (C) 2004 Laurence Reading

#    This file is part of ProjectAwesomeModBot.
#
#    ProjectAwesomeModBot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ProjectAwesomeModBot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ProjectAwesomeModBot.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import logging
import requests
import RedditAPIFacade

__author__ = 'Laurence'


def replaceBetween(old, startTag, endTag, new):
    start = old.find(startTag)
    if (start == -1):
        raise RuntimeError("Unable to find start tag in current sidebar")
    start += startTag.__len__()
    end = old.find(endTag)
    if (end == -1):
        raise RuntimeError("Unable to find end tag (" + endTag + ") in current sidebar")
    return (old[:start] + new + old[end:])


def compareBetween(old, startTag, endTag, new):
    start = old.find(startTag)
    if (start == -1):
        raise RuntimeError("Unable to find start tag in current sidebar")
    start += startTag.__len__()
    end = old.find(endTag)
    if (end == -1):
        raise RuntimeError("Unable to find end tag (" + endTag + ") in current sidebar")
    return (old[start:end] == new)


def findEventDetails(data, calendarDetailsTag, dateFormat):
    logger = logging.getLogger("redditbot")
    events = []
    errors = []
    posts = data.json()['data']['children']
    for post in posts:
        data = post['data']
        try:
            if (data['link_flair_css_class'].upper() == "EVENT".upper()):
                #Search their text for calendar details and store it
                selftext = data['selftext']
                location = selftext.find(calendarDetailsTag)
                if (location == -1):
                    logger.error("Unable to find Calendar Details in post: " + data['name'])
                    errors.append(data['name'])
                while (location != -1):
                    split = selftext[location:].split('|')
                    datestr = split[1]
                    game = split[2]
                    title = split[3].split('\n')[0].strip()
                    postID = data['id']
                    subreddit = data['subreddit']
                    datestr = datestr.replace("24:00","23:59")
                    date = datetime.datetime.strptime(datestr, dateFormat)
                    events.append({'date': date, 'title': title, 'url': data['url'], 'game': game, 'postID': postID, 'subreddit':subreddit})
                    selftext = selftext[location + calendarDetailsTag.__len__():]
                    location = selftext.find(calendarDetailsTag)
        except:
            logger.exception("Error while trying to find Calendar Details in post")
            errors.append(data['name'])
    return {'events': events, 'errors': errors}


def isPostCommentedOn(username, postID):
    comments = RedditAPIFacade.getComments(username)
    return comments.text.find(postID) != -1




